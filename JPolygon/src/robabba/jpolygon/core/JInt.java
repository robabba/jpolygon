/** Java Polygon JInt Class
    Copyright (C) 2014 Robert Abba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package robabba.jpolygon.core;

public class JInt {

	private int i;

	public JInt (int integer)
	{
		i = integer;
	}
	
	public JInt(){ /*...*/ }
	
	public int value()
	{
		return i;
	}
	
	public void setValue(int integer)
	{
		i = integer;
	}
	
	public static int tryParse(JString s)
	{
		String integer = "";
		
		boolean failedToParse = false;
		
		for(int i = 0; i <= s.length() - 1; i++)
		{
			switch(s.charAtIndex(i))
			{
			case '0':
				integer += "0";
				break;
			case '1':
				integer += "1";
				break;
			case '2':
				integer += "2";
				break;
			case '3':
				integer += "3";
				break;
			case '4':
				integer += "4";
				break;
			case '5':
				integer += "5";
				break;
			case '6':
				integer += "6";
				break;
			case '7':
				integer += "7";
				break;
			case '8':
				integer += "8";
				break;
			case '9':
				integer += "9";
				break;
			default:
				failedToParse = true;
				break;
			}
		}
		
		if (!failedToParse)
			return Integer.parseInt(integer);
		else
			Console.writeln("Failed to parse.");
		
		return 0;
	}
}
