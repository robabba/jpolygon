/** Java Polygon Any Class
    Copyright (C) 2014 Robert Abba

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package robabba.jpolygon.core;

public class Any extends Object{ 

	private Any a = null;
	private ParseType t = null;
	
	public enum ParseType
	{
		INTEGER,
		STRING,
		ANY,
		DOUBLE,
		FLOAT,
		BYTE
	}
	
	public Any(int value) {
		a = TryParseAnyFromInt(value);
		t = ParseType.INTEGER;
	}
	
	public Any(String value){
		a = TryParseAnyFromString(value);
		t = ParseType.STRING;
	}
	
	public Any(Any value){
		a = TryParseAnyFromAny(value);
		t = ParseType.ANY;
	}
	
	public Object value()
	{
		switch (t)
		{
			case INTEGER:
				return TryParseIntegerFromAny(a);
		}
		
		return null;
	}
	
	public Any TryParseAnyFromAny(Any value)
	{
		Any parsedValue = null;
		try{
			parsedValue = parseFromAny(value, ParseType.ANY);
		}catch (Exception e){
			Console.out(e.getMessage());
		}
		
		return parsedValue;
	}
	
	public Any TryParseAnyFromInt(int value)
	{
		Any parsedValue = null;
		try{
			parsedValue = parseFromAny(value, ParseType.ANY);
		}catch (Exception e){
			Console.out(e.getMessage());
		}
		
		return parsedValue;
	}
	
	public Any TryParseAnyFromString(String value)
	{
		Any parsedValue = null;
		try{
			parsedValue = parseFromAny(value, ParseType.ANY);
		}catch (Exception e){
			Console.out(e.getMessage());
		}
		
		return parsedValue;
	}
	
	// Reverse Try
	public int TryParseIntegerFromAny(Any value)
	{
		int parsedValue = 0;
		try{
			parsedValue = parseFromAny(value, ParseType.INTEGER);
		}catch (Exception e){
			Console.out(e.getMessage());
		}
		
		return parsedValue;
	}
	
	private <T> T parseFromAny(Object o, ParseType p) throws Exception{
		Class<T> c = null;
		
		switch (p){
		case INTEGER:
			return c.getConstructor(new Class[] {Integer.class}).newInstance(o);
		case STRING:
			return c.getConstructor(new Class[] {String.class}).newInstance(o);
		case ANY:
			return c.getConstructor(new Class[] {Any.class}).newInstance(o);
		case DOUBLE:
			return c.getConstructor(new Class[] {Double.class}).newInstance(o);
		case FLOAT:
			return c.getConstructor(new Class[] {Float.class}).newInstance(o);
		case BYTE:
			return c.getConstructor(new Class[] {Byte.class}).newInstance(o);
		}
		
		return null;
	}
	
	
}
