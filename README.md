# JPolygon #

A Java wrapper based on Polygon.

### Getting Started with JPolygon ###
* Download and add JPolygonWrapper.jar to your project
* To add it to your file, type:
	
```java
import robabba.jpolygon.core.*;
```

* Then you can start using it in your file!

### Basic uses ###

Why write longer, when you can write shorter?

```java
// Instead of "System.out.println("Hello, World");" Write:
Console.writeln("Hello, World");

// Instead of "System.out.println("Hello " + name + ".How are you doing today " + name "?");" Write:
Console.writeln("Hello {0}. How are you doing today {0}", name);
```

### Custom Datatypes ###
Because Java can't pass by reference using primative datatypes, there are some custom datatypes being used.

```java
JString name = new JString("Robert"); // Similar to String
JInt age = new JInt(); // Similar to int;

Console.write("Hello {0}! How old are you?: ");
Console.read(age);	// Input: 20

Console.write("{0} is {1} years old", name.value(), age.value()); 
// Output: Robert is 20 years old
```

Licensed under GNU GPL v3 License.